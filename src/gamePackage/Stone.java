package gamePackage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Stone extends Block implements Animatable{

    Stone (int x, int y){
        super(x, y);
    }


    @Override
    public void step() {
        animation.animate();
    }
    @Override
    public void save(BufferedWriter writer) {
        String str = "Stone" + "#" + this.location.getX() + "#" + this.location.getY() + "#\n";
        try { writer.write(str); } catch (IOException e) { e.printStackTrace(); }
    }

}
