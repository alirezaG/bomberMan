package gamePackage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Bomb extends Block implements Animatable {
    private int explosionTime, burningTime;
    private int explosionRange, startInCol, endInCol, startInRow, endInRow; // Note: startInCol : the beginning of the explosion interval in the *DIRECTION* of column
    private final Map map;
    private final Player bomber;

    private BombStatus status;

    Bomb(int x, int y, Map map, Player bomber, int explosionTime, int explosionRange) {
        super(x, y);
        this.map = map;
        this.bomber = bomber;
        this.explosionTime = explosionTime;
        this.explosionRange = explosionRange;
        this.burningTime = explosionRange * 200;  // default burningTime
        this.location = new Point(x, y);
        status = BombStatus.Waiting;
    }

    @Override
    public void step() {
        animation.animate();
    }

    @Override
    public void save(BufferedWriter writer) {
        String str = "Bomb" + "#" + this.location.getX() + "#" + this.location.getY() + "#" + this.explosionTime + "#" +
                this.explosionRange + "#" + this.startInCol + "#" + this.endInCol + "#" + this.startInRow + "#"  +
                this.endInRow + "#\n";
        try { writer.write(str); } catch (IOException e) { e.printStackTrace(); }
    }



    public void decreaseTime(int d){
        if (status != BombStatus.Burning)
            explosionTime -= d;
        else
            burningTime -= d;
        if (getExplosionTime() <= 0 && getStatus() != BombStatus.Burning)
            explode();
    }



    public void explode(){
        status = BombStatus.Burning;
        // Now we have to determine which cells should get exploded
        int start, end;
        start = location.getX();
        for (int range = explosionRange; map.getBlock(start, location.getY()) instanceof Stone == false && range >= 0; range--) {
            start--;
        }
        start++; // to make [] interval
        startInCol = start;
        end = location.getX();
        for (int range = explosionRange; map.getBlock(end, location.getY()) instanceof Stone == false && range >= 0; range--)
            end++;
        end--; // to make [] interval
        endInCol = end;

        start = location.getY();
        for (int range = explosionRange; map.getBlock(location.getX(), start) instanceof Stone == false && range >= 0; range--)
            start--;
        start++; // to make [] interval
        startInRow = start;
        end = location.getY();
        for (int range = explosionRange; map.getBlock(location.getX(), end) instanceof Stone == false && range >= 0; range--)
            end++;
        end--; // to make [] interval
        endInRow = end;


        explodeBlocks();
        explodePlayers();
        explodeAliens();
    }



    private void explodeBlocks(){
        int bX = this.location.getX(), bY = this.location.getY();
        // NOTE: explosion in the direction of COLUMNS
        for (int i = startInCol; i <= endInCol; i++) {
            EmptyBlock emptyBlock = new EmptyBlock(i, bY, map);
            map.setBlock(i, bY, emptyBlock);
            emptyBlock.setDelayToExplosionCenter(Math.abs(i - bX) * map.getDelayTimeForAdjacentBombExplosion());

            if (i == startInCol && i != endInCol)
                emptyBlock.setStatus(EmptyBlockStatus.upEdgeExplosion);
            else if (i == endInCol && i != startInCol)
                emptyBlock.setStatus(EmptyBlockStatus.downEdgeExplosion);
            else
                emptyBlock.setStatus(EmptyBlockStatus.normalExplosion);
        }

        // now explosion in the direction of ROWS
        for (int j = startInRow; j <= endInRow; j++) {
            EmptyBlock emptyBlock = new EmptyBlock(bX, j, map);
            map.setBlock(bX, j, emptyBlock);
            emptyBlock.setDelayToExplosionCenter(Math.abs(j - bY) * map.getDelayTimeForAdjacentBombExplosion());

            if (j == startInRow && j != endInRow)
                emptyBlock.setStatus(EmptyBlockStatus.leftEdgeExplosion);
            else if (j == endInRow && j != startInRow)
                emptyBlock.setStatus(EmptyBlockStatus.rightEdgeExplosion);
            else
                emptyBlock.setStatus(EmptyBlockStatus.normalExplosion);
        }

        ((EmptyBlock)map.getBlock(location.getX(), location.getY())).setStatus(EmptyBlockStatus.normalExplosion);
    }


    private void explodePlayers(){
        for (int i = map.getNumberOfPlayers() - 1; i >= 0; i--){     // backward loop
            Player player = map.getPlayer(i);
            if (player.location.getX() == this.location.getX() && startInRow <= player.location.getY() &&
                    player.location.getY() <= endInRow)
               ;// map.killPlayer(i);

            else if (player.location.getY() == this.location.getY() && startInCol <= player.location.getX() &&
                    player.location.getX() <= endInCol)
                ;//map.killPlayer(i);
        }
    }


    private void explodeAliens(){
        for (int i = map.getNumberOfAliens() - 1; i >= 0; i--){    // backward loop
            Alien alien = map.getAlien(i);
            if (alien.location.getX() == this.location.getX() && startInRow <= alien.location.getY() &&
                    alien.location.getY() <= endInRow)
                map.killAlien(i);

            else if (alien.location.getY() == this.location.getY() && startInCol <= alien.location.getX() &&
                    alien.location.getX() <= endInCol)
                map.killAlien(i);
        }
    }



    public void finish(){
        bomber.decreaseNumberOfPlantedBombs();  // you should put it here, not in explode(), because then function player.getNumberOfPlantedBombs() will return one
                                                // less and for that bomb we will never call finish() function
        int bX = this.location.getX(), bY = this.location.getY();
        for (int i = startInCol; i <= endInCol; i++) {
            ((EmptyBlock) map.getBlock(i, bY)).setStatus(EmptyBlockStatus.normal);
            ((EmptyBlock) map.getBlock(i, bY)).setDelayToExplosionCenter(0);
        }
        System.out.println(startInRow + "  #  " + endInRow);
        for (int j = startInRow; j <= endInRow; j++) {
            ((EmptyBlock) map.getBlock(bX, j)).setStatus(EmptyBlockStatus.normal);
            ((EmptyBlock) map.getBlock(bX, j)).setDelayToExplosionCenter(0);
        }

        updatePowersVisibility();
    }


    private void updatePowersVisibility() {
        int bX = this.location.getX(), bY = this.location.getY();
        for (int i = map.getNumberOfPowers() - 1; i >= 0; i--){
            PowerUp power = map.getPower(i);
            if (power.location.getX() == bX && startInRow <= power.location.getY() && power.location.getY() <= endInRow)
                power.setVisible(true);
            else if (power.location.getY() == bY && startInCol <= power.location.getX() && power.location.getX() <= endInCol)
                power.setVisible(true);
        }
    }


    // Setters amd Getters
    public int getExplosionTime(){
        synchronized (this){
            return explosionTime;
        }
    }

    public int getBurningTime(){
        synchronized (this){
            return burningTime;
        }
    }

    public BombStatus getStatus(){
        synchronized (this){
            return status;
        }
    }
}
