package gamePackage;

public enum Direction {
    Right, Left, Up, Down, Undefined
}
