package gamePackage;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class GameFrame extends JFrame {
    private final StartFrame startFrame;
    private JPanel contentPane;
    private JPanel toolPane;
    private PaintPanel paintPanel;
    private JProgressBar timeProgressBar;
    private JLabel timeLabel, levelLabel;

    Thread timeThread;


    GameFrame(StartFrame startFrame, BufferedReader reader){
        this.startFrame = startFrame;

        try {
            paintPanel = new PaintPanel(this, new Dimension(this.getWidth() - 85, this.getHeight() - 100), reader);
        } catch (IOException e) {e.printStackTrace();}

        initialize();
    }

    GameFrame(StartFrame startFrame, int numberOfRows, int numberOfCols, int numberOfPlayers, int numberOfAliens){
        this.startFrame = startFrame;
        paintPanel = new PaintPanel(this, new Dimension(this.getWidth() - 85, this.getHeight() - 100), numberOfRows, numberOfCols,
                numberOfPlayers, numberOfAliens);
        initialize();
    }


/*
        Adding maps for different Levels
        Save maps somewhere in data, and then use them.
        just save 4 maps
        adding exit door
        adding score decreasing as time goes by
 */

    private void initialize(){
        this.setSize(new Dimension(1400, 730));
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                startFrame.setGameStarted(false);
                timeThread.stop();
                paintPanel.stopThreads();
            }
        });
        ImageIcon iconImg = new ImageIcon("./data/bomberManWithShield2.jpg");
        this.setIconImage(iconImg.getImage());
        this.setTitle("Game");

        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(new BorderLayout());
        toolPane = new JPanel(new GridLayout(1, 3));
        toolPane.setPreferredSize(new Dimension(this.getWidth(), 50));

        levelLabel = new JLabel();

        GameFrame thisFrame = this;  // for inside anonymous classes we need this variable
        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) { }
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_DOWN)
                    paintPanel.keyPressed(e);
            }
            @Override
            public void keyReleased(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_DOWN
                        || keyCode == KeyEvent.VK_B)
                    paintPanel.keyReleased(e);

                else if (keyCode == KeyEvent.VK_S && e.isControlDown()) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setDialogTitle("Specify a file to save");
                    FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Text Files","txt");
                    fileChooser.addChoosableFileFilter(fileFilter);
                    int userSelection = fileChooser.showSaveDialog(thisFrame);

                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                        File savedFile = fileChooser.getSelectedFile();
                        BufferedWriter writer = null;
                        try {
                            writer = new BufferedWriter(new FileWriter(savedFile));
                            paintPanel.getMap().saveAll(writer);
                        }
                        catch (IOException e1) { e1.printStackTrace(); }
                        finally {
                            try { writer.close(); } catch (IOException e2) {e2.printStackTrace();}
                        }
                    }
                }

                else if (keyCode == KeyEvent.VK_O && e.isControlDown()) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setDialogTitle("Open your file to load the game");
                    FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Text Files","txt");
                    fileChooser.addChoosableFileFilter(fileFilter);
                    int userSelection = fileChooser.showOpenDialog(thisFrame);

                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                        File openedFile = fileChooser.getSelectedFile();
                        BufferedReader reader = null;
                        try {
                            reader = new BufferedReader(new FileReader(openedFile));
                            try {
                                paintPanel.setMap(new Map(reader));
                                paintPanel.initialize();
                                levelLabel.setText("level: " + String.valueOf(paintPanel.getMap().getLevel()));

                            } catch (IOException IOExp){IOExp.printStackTrace();}
                        } catch (FileNotFoundException e1) { e1.printStackTrace(); }
                        finally {
                            try { reader.close(); } catch (IOException e1) { e1.printStackTrace(); }
                        }
                    }
                }
            }
        });



        timeLabel = new JLabel("Time: 500 s");
        timeProgressBar = new JProgressBar();
        timeProgressBar.setMinimum(0);
        timeProgressBar.setMaximum(500);
        timeProgressBar.setValue(500);
        timeProgressBar.setForeground(Color.BLUE);

        timeThread = new Thread(new Runnable() {
            int remainingTime = 500;
            boolean running = true;
            @Override
            public void run() {
                while (running && remainingTime > 0) {
                    try {
                        Thread.sleep(1000);
                        remainingTime--;
                        timeLabel.setText("Time: " + remainingTime + " s");
                        timeProgressBar.setValue(remainingTime);
                    } catch (InterruptedException e) { e.printStackTrace();}
                }
                thisFrame.dispatchEvent(new WindowEvent(thisFrame, WindowEvent.WINDOW_CLOSING));
            }
        });
        timeThread.start();

        toolPane.add(timeLabel);
        toolPane.add(timeProgressBar);
        toolPane.add(levelLabel);
        contentPane.add(toolPane, BorderLayout.NORTH);
        contentPane.add(paintPanel, BorderLayout.CENTER);
        paintPanel.setFocusable(true);
        paintPanel.requestFocusInWindow();
        this.setVisible(true);
    }


    // other functions
    public void close(){
        startFrame.setGameStarted(false);
        timeThread.stop();
        paintPanel.stopThreads();
        this.setVisible(false);
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

