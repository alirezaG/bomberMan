package gamePackage;

public interface Animatable {
    public void step();
}
