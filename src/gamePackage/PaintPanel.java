package gamePackage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.ImageObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class PaintPanel extends JPanel implements KeyListener{
    private Map map;

    private Image[] allImages;
    private ArrayList<String> imageAddresses;
    private DisplayImage[][] blockImages;
    private DisplayImage[] playerImages;
    private ArrayList<DisplayImage> bombImages;
    private ArrayList<DisplayImage> alienImages;
    private ArrayList<DisplayImage> powerImages;

    private Thread animationThread;
    private final int animationThreadTime = 20;

    private final int cellSize = 35;
    private Date keyboardDate;
    private final GameFrame gameFrame;
    
    private AdvancedAnimation playerMoveRightAnimation, playerMoveLeftAnimation, playerMoveUpAnimation, playerMoveDownAnimation, playerStayAnimation,
        emptyBlockAnimation, wallAnimation, stoneAnimation,
    alienMoveDownAnimationType[], alienMoveLeftAnimationType[], alienMoveRightAnimationType[], alienMoveUpAnimationType[], alienStayAnimationType[],
            alienDeadAnimationType[],
    normalExplosionAnimation, downEdgeExplosionAnimation, upEdgeExplosionAnimation, leftEdgeExplosionAnimation, rightEdgeExplosionAnimation,
    powerUpIncreaseSpeedAnimation, powerUpIncreasePointsAnimation, powerUpIncreaseRadiusAnimation, powerUpIncreaseBombsAnimation, powerUpControlBombsAnimation,
    powerUpDecreaseSpeedAnimation, powerUpDecreasePointsAnimation, powerUpDecreaseRadiusAnimation, powerUpDecreaseBombsAnimation;



    PaintPanel(GameFrame gameFrame, Dimension D, int numberOfRows, int numberOfCols, int numberOfPlayers, int numberOfAliens){
        this.setPreferredSize(D);
        this.gameFrame = gameFrame;

        map = new Map(numberOfRows, numberOfCols, numberOfPlayers, numberOfAliens);
        keyboardDate = null;
        initialize();
    }

    PaintPanel(GameFrame gameFrame, Dimension D, BufferedReader reader) throws IOException {
        this.setPreferredSize(D);
        this.gameFrame = gameFrame;
        map = new Map(reader);
        keyboardDate = null;
        initialize();
    }




    // View functions

    void initialize(){
        initializeImages();
        initializeAnimations();

        for (int i = 0; i < map.getNumberOfPlayers(); i++){
            map.getPlayer(i).setAnimation(playerStayAnimation);
        }
        for (int i = 0; i < map.getNumberOfRows(); i++)
            for (int j = 0; j < map.getNumberOfCols(); j++) {
                if (map.getBlock(i, j) instanceof EmptyBlock)
                    map.getBlock(i, j).setAnimation(emptyBlockAnimation);
                else if (map.getBlock(i, j) instanceof Stone)
                    map.getBlock(i, j).setAnimation(stoneAnimation);
                else if (map.getBlock(i, j) instanceof Wall)
                    map.getBlock(i, j).setAnimation(wallAnimation);
            }

        blockImages = new DisplayImage[map.getNumberOfRows()][map.getNumberOfCols()];
        playerImages = new DisplayImage[map.getNumberOfPlayers()];
        this.setBackground(Color.BLACK);

        for (int i = 0; i < map.getNumberOfRows(); i++)
            for (int j = 0; j < map.getNumberOfCols(); j++) {
                blockImages[i][j] = new DisplayImage(new Point(j*cellSize, i*cellSize));  // Note: i, j must be substituted because the x-axis in screen in different
                blockImages[i][j].setImage(allImages[map.getBlock(i, j).animation.getCurrentImageIndex()]);
            }
        for (int i = 0; i < map.getNumberOfPlayers(); i++) {
            playerImages[i] = new DisplayImage(new Point(0, 0));
        }

        powerImages = new ArrayList<>();
        for (int i = map.getNumberOfPowers() - 1; i >= 0; i--) {   // backward loop
            PowerUp power = map.getPower(i);
            if (power.getType() == PowerUpType.ControlBomb)
                power.setAnimation(powerUpControlBombsAnimation);
            else if (power.getType() == PowerUpType.IncreaseBombs)
                power.setAnimation(powerUpIncreaseBombsAnimation);
            else if (power.getType() == PowerUpType.IncreasePoints)
                power.setAnimation(powerUpIncreasePointsAnimation);
            else if (power.getType() == PowerUpType.IncreaseRadius)
                power.setAnimation(powerUpIncreaseRadiusAnimation);
            else if (power.getType() == PowerUpType.IncreaseSpeed)
                power.setAnimation(powerUpIncreaseSpeedAnimation);

            else if (power.getType() == PowerUpType.DecreaseBombs)
                power.setAnimation(powerUpDecreaseBombsAnimation);
            else if (power.getType() == PowerUpType.DecreasePoints)
                power.setAnimation(powerUpDecreasePointsAnimation);
            else if (power.getType() == PowerUpType.DecreaseRadius)
                power.setAnimation(powerUpDecreaseRadiusAnimation);
            else if (power.getType() == PowerUpType.DecreaseSpeed)
                power.setAnimation(powerUpDecreaseSpeedAnimation);
        }


        animationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!map.isFinished()){
                    try {
                        map.semaphore.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    animateAll();
                    reloadImages();
                    repaint();
                    map.semaphore.release();

                    try {
                        Thread.sleep(animationThreadTime);
                    } catch (InterruptedException e) { e.printStackTrace(); }
                }

                JOptionPane.showMessageDialog(gameFrame, "You have lost!", "Oh no!", JOptionPane.INFORMATION_MESSAGE);
                gameFrame.close();
            }
        });
        animationThread.start();
    }



    // animation functions
    private void animateAll(){
        animateBlocks();
        animatePowers();
        animateBombs();
        animateAliens();
        animatePlayers();
    }

    private void animateBlocks() {
        for (int i = 0; i < map.getNumberOfRows(); i++)
            for (int j = 0; j < map.getNumberOfCols(); j++) {
                if (map.getBlock(i, j) instanceof EmptyBlock) {  // only EmptyBlocks might get affected by bombs
                    loadEmptyBlockAppropriateAnimation(i, j);
                }
                map.getBlock(i, j).step();
            }
    }

    private void animatePowers(){
        for (int i = 0; i < map.getNumberOfPowers(); i++)
            map.getPower(i).step();
    }

    private void animatePlayers(){
        for (int i = 0; i < map.getNumberOfPlayers(); i++){
            if (map.getPlayer(i).getPlayerStatus() == PlayerStatus.Dead)
                continue;
            map.getPlayer(i).step();
        }
    }

    private void animateBombs(){
        for (int i = 0; i < map.getNumberOfPlayers(); i++) {
            for (int j = 0; j < map.getPlayer(i).getNumberOfPlantedBombs(); j++) {
                Bomb bomb = map.getPlayer(i).getBomb(j);
                if (bomb.animation == null) {  // recently planted
                    ArrayList<Integer> bombImageIndices = new ArrayList<>();
                    bombImageIndices.add(imageAddresses.indexOf("./data/bomb3.JPG"));
                    bombImageIndices.add(imageAddresses.indexOf("./data/bomb2.JPG"));
                    bombImageIndices.add(imageAddresses.indexOf("./data/bomb1.JPG"));
                    AdvancedAnimation bombAnimation = new AdvancedAnimation(bomb.getExplosionTime() / 3, bombImageIndices);
                    bomb.setAnimation(bombAnimation);
                }
                bomb.step();
            }
        }
    }

    private void animateAliens(){
        for (int i = 0; i < map.getNumberOfAliens(); i++) {
            Alien alien = map.getAlien(i);
            // no need to check the condition (alien.status == Disappeared) because it gets removed from the map.aliens[]
            if (alien.getStatus() != alien.getPreviousStatus())
                loadAlienAppropriateAnimation(alien);
            alien.step();
        }
    }




    private void initializeImages(){
        imageAddresses = new ArrayList<>();
        // player stay
        imageAddresses.add("./data/bomberManStandStill.JPG");
        imageAddresses.add("./data/bomberManStandLeft.JPG");
        imageAddresses.add("./data/bomberManStandLeftHandUp.JPG");
        imageAddresses.add("./data/bomberManStandLeft.JPG");
        imageAddresses.add("./data/bomberManStandLeftHandUp.JPG");
        imageAddresses.add("./data/bomberManStandLeft.JPG");
        imageAddresses.add("./data/bomberManStandStill.JPG");
        imageAddresses.add("./data/bomberManSmallEye.JPG");
        imageAddresses.add("./data/bomberManSmallEye.JPG");
        imageAddresses.add("./data/bomberManStandStill.JPG");
        imageAddresses.add("./data/bomberManHandsSide.JPG");
        imageAddresses.add("./data/bomberManHandsUp.JPG");
        imageAddresses.add("./data/bomberManHandsSide.JPG");
        imageAddresses.add("./data/bomberManHandsUp.JPG");
        imageAddresses.add("./data/bomberManStandStill.JPG");
        imageAddresses.add("./data/bomberManStandLeft.JPG");
        imageAddresses.add("./data/bomberManStandLeftStop.JPG");
        imageAddresses.add("./data/bomberManStandLeftStop.JPG");
        imageAddresses.add("./data/bomberManSmallEye.JPG");
        imageAddresses.add("./data/bomberManStandLeftStop.JPG");
        imageAddresses.add("./data/bomberManStandLeftStop.JPG");
        imageAddresses.add("./data/bomberManStandStill.JPG");
        // player right
        imageAddresses.add("./data/bomberManMoveRight1.JPG");
        imageAddresses.add("./data/bomberManMoveRight2.JPG");
        imageAddresses.add("./data/bomberManMoveRight3.JPG");
        // player left
        imageAddresses.add("./data/bomberManMoveLeft1.JPG");
        imageAddresses.add("./data/bomberManMoveLeft2.JPG");
        imageAddresses.add("./data/bomberManMoveLeft3.JPG");
        // player down
        imageAddresses.add("./data/bomberManHandsSide.JPG");
        imageAddresses.add("./data/bomberManMoveDown.JPG");
        imageAddresses.add("./data/bomberManMoveDown.JPG");
        imageAddresses.add("./data/bomberManStandStill.JPG");
        // player up
        imageAddresses.add("./data/bomberManMoveUp.JPG");
        // empty block
        imageAddresses.add("./data/emptyBlock.JPG");
        // wall
        imageAddresses.add("./data/wall.JPG");
        // stone
        imageAddresses.add("./data/stone.JPG");
        
        // bomb
        imageAddresses.add("./data/bomb3.JPG");
        imageAddresses.add("./data/bomb2.JPG");
        imageAddresses.add("./data/bomb1.JPG");
        
        // bomb explosion
        imageAddresses.add("./data/normalExp.JPG");
        imageAddresses.add("./data/downEdgeExp.JPG");
        imageAddresses.add("./data/upEdgeExp.JPG");
        imageAddresses.add("./data/leftEdgeExp.JPG");
        imageAddresses.add("./data/rightEdgeExp.JPG");

        // alien type1
        imageAddresses.add("./data/alienType1MoveDown1.png");
        imageAddresses.add("./data/alienType1MoveDown2.png");
        
        imageAddresses.add("./data/alienType1MoveLeft1.png");
        imageAddresses.add("./data/alienType1MoveLeft2.png");
        imageAddresses.add("./data/alienType1MoveLeft3.png");

        imageAddresses.add("./data/alienType1MoveRight1.png");
        imageAddresses.add("./data/alienType1MoveRight2.png");
        imageAddresses.add("./data/alienType1MoveRight3.png");

        imageAddresses.add("./data/alienType1MoveUp1.png");
        imageAddresses.add("./data/alienType1MoveUp2.png");

        imageAddresses.add("./data/alienType1Stay1.png");
        imageAddresses.add("./data/alienType1Stay2.png");
        
        imageAddresses.add("./data/alienType1Dying1.png");
        imageAddresses.add("./data/alienType1Dying2.png");
        imageAddresses.add("./data/alienType1Dying3.png");

        // alien type2
        imageAddresses.add("./data/alienType2MoveDown1.png");
        imageAddresses.add("./data/alienType2MoveDown2.png");

        imageAddresses.add("./data/alienType2MoveLeft1.png");
        imageAddresses.add("./data/alienType2MoveLeft2.png");
        imageAddresses.add("./data/alienType2MoveLeft3.png");

        imageAddresses.add("./data/alienType2MoveRight1.png");
        imageAddresses.add("./data/alienType2MoveRight2.png");
        imageAddresses.add("./data/alienType2MoveRight3.png");

        imageAddresses.add("./data/alienType2MoveUp1.png");
        imageAddresses.add("./data/alienType2MoveUp2.png");

        imageAddresses.add("./data/alienType2Stay1.png");
        imageAddresses.add("./data/alienType2Stay2.png");

        imageAddresses.add("./data/alienType2Dying1.png");
        imageAddresses.add("./data/alienType2Dying2.png");
        imageAddresses.add("./data/alienType2Dying3.png");


        // alien type3
        imageAddresses.add("./data/alienType3MoveDown1.png");
        imageAddresses.add("./data/alienType3MoveDown2.png");

        imageAddresses.add("./data/alienType3MoveLeft1.png");
        imageAddresses.add("./data/alienType3MoveLeft2.png");
        imageAddresses.add("./data/alienType3MoveLeft3.png");

        imageAddresses.add("./data/alienType3MoveRight1.png");
        imageAddresses.add("./data/alienType3MoveRight2.png");
        imageAddresses.add("./data/alienType3MoveRight3.png");

        imageAddresses.add("./data/alienType3MoveUp1.png");
        imageAddresses.add("./data/alienType3MoveUp2.png");

        imageAddresses.add("./data/alienType3Stay1.png");
        imageAddresses.add("./data/alienType3Stay2.png");

        imageAddresses.add("./data/alienType3Dying1.png");
        imageAddresses.add("./data/alienType3Dying2.png");
        imageAddresses.add("./data/alienType3Dying3.png");

        // alien type4
        imageAddresses.add("./data/alienType4MoveDown1.png");
        imageAddresses.add("./data/alienType4MoveDown2.png");

        imageAddresses.add("./data/alienType4MoveLeft1.png");
        imageAddresses.add("./data/alienType4MoveLeft2.png");
        imageAddresses.add("./data/alienType4MoveLeft3.png");

        imageAddresses.add("./data/alienType4MoveRight1.png");
        imageAddresses.add("./data/alienType4MoveRight2.png");
        imageAddresses.add("./data/alienType4MoveRight3.png");

        imageAddresses.add("./data/alienType4MoveUp1.png");
        imageAddresses.add("./data/alienType4MoveUp2.png");

        imageAddresses.add("./data/alienType4Stay1.png");
        imageAddresses.add("./data/alienType4Stay2.png");

        imageAddresses.add("./data/alienType4Dying1.png");
        imageAddresses.add("./data/alienType4Dying2.png");
        imageAddresses.add("./data/alienType4Dying3.png");

        // power-ups
        imageAddresses.add("./data/powerUpControlBombs.png");
        imageAddresses.add("./data/powerUpIncreaseBombs.png");
        imageAddresses.add("./data/powerUpIncreasePoints.png");
        imageAddresses.add("./data/powerUpIncreaseRadius.png");
        imageAddresses.add("./data/powerUpIncreaseSpeed.png");

        imageAddresses.add("./data/powerUpDecreaseBombs.png");
        imageAddresses.add("./data/powerUpDecreasePoints.png");
        imageAddresses.add("./data/powerUpDecreaseRadius.png");
        imageAddresses.add("./data/powerUpDecreaseSpeed.png");


        // filling image array
        allImages = new Image[imageAddresses.size()];
        for (int i = 0; i < imageAddresses.size(); i++) {
            try {
                File imageFile = new File(imageAddresses.get(i));
                allImages[i] = ImageIO.read(imageFile);
            } catch (IOException e) { e.printStackTrace();}
        }

    }



    private void initializeAnimations() {
        // Player
        ArrayList<Integer> playerStayImageIndices = new ArrayList<>();
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeft.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftHandUp.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeft.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftHandUp.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeft.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManSmallEye.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManSmallEye.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManHandsSide.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManHandsUp.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManHandsSide.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManHandsUp.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeft.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftStop.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftStop.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManSmallEye.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftStop.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandLeftStop.JPG"));
            playerStayImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
        playerStayAnimation = new AdvancedAnimation(200, playerStayImageIndices);

        ArrayList<Integer> playerMoveRightImageIndices = new ArrayList<>();
            playerMoveRightImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveRight1.JPG"));
            playerMoveRightImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveRight2.JPG"));
            playerMoveRightImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveRight3.JPG"));
        playerMoveRightAnimation = new AdvancedAnimation(100, playerMoveRightImageIndices);

        ArrayList<Integer> playerMoveLeftImageIndices = new ArrayList<>();
            playerMoveLeftImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveLeft1.JPG"));
            playerMoveLeftImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveLeft2.JPG"));
            playerMoveLeftImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveLeft3.JPG"));
        playerMoveLeftAnimation = new AdvancedAnimation(100, playerMoveLeftImageIndices);

        ArrayList<Integer> playerMoveDownImageIndices = new ArrayList<>();
            playerMoveDownImageIndices.add(imageAddresses.indexOf("./data/bomberManHandsSide.JPG"));
            playerMoveDownImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveDown.JPG"));
            playerMoveDownImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveDown.JPG"));
            playerMoveDownImageIndices.add(imageAddresses.indexOf("./data/bomberManStandStill.JPG"));
        playerMoveDownAnimation = new AdvancedAnimation(100, playerMoveDownImageIndices);

        ArrayList<Integer> playerMoveUpImageIndices = new ArrayList<>();
            playerMoveUpImageIndices.add(imageAddresses.indexOf("./data/bomberManMoveUp.JPG"));
        playerMoveUpAnimation = new AdvancedAnimation(100, playerMoveUpImageIndices);


        // Empty Block
        ArrayList<Integer> emptyBlockImageIndices = new ArrayList<>();
            emptyBlockImageIndices.add(imageAddresses.indexOf("./data/emptyBlock.JPG"));
        emptyBlockAnimation = new AdvancedAnimation(100, emptyBlockImageIndices);

        // Wall
        ArrayList<Integer> wallImageIndices = new ArrayList<>();
            wallImageIndices.add(imageAddresses.indexOf("./data/wall.JPG"));
        wallAnimation = (new AdvancedAnimation(100, wallImageIndices));

        // Stone
        ArrayList<Integer> stoneImageIndices = new ArrayList<>();
            stoneImageIndices.add(imageAddresses.indexOf("./data/stone.JPG"));
        stoneAnimation  = new AdvancedAnimation(100, stoneImageIndices);
        
        
        
        // Bomb explosion
        ArrayList<Integer> normalExplosionImageIndices = new ArrayList<>();
            normalExplosionImageIndices.add(imageAddresses.indexOf("./data/normalExp.JPG"));
        normalExplosionAnimation = new AdvancedAnimation(100, normalExplosionImageIndices);

        ArrayList<Integer> downEdgeExplosionImageIndices = new ArrayList<>();
            downEdgeExplosionImageIndices.add(imageAddresses.indexOf("./data/downEdgeExp.JPG"));
        downEdgeExplosionAnimation = new AdvancedAnimation(100, downEdgeExplosionImageIndices);

        ArrayList<Integer> upEdgeExplosionImageIndices = new ArrayList<>();
            upEdgeExplosionImageIndices.add(imageAddresses.indexOf("./data/upEdgeExp.JPG"));
        upEdgeExplosionAnimation = new AdvancedAnimation(100, upEdgeExplosionImageIndices);

        ArrayList<Integer> leftEdgeExplosionImageIndices = new ArrayList<>();
            leftEdgeExplosionImageIndices.add(imageAddresses.indexOf("./data/leftEdgeExp.JPG"));
        leftEdgeExplosionAnimation = new AdvancedAnimation(100, leftEdgeExplosionImageIndices);

        ArrayList<Integer> rightEdgeExplosionImageIndices = new ArrayList<>();
            rightEdgeExplosionImageIndices.add(imageAddresses.indexOf("./data/rightEdgeExp.JPG"));
        rightEdgeExplosionAnimation = new AdvancedAnimation(100, rightEdgeExplosionImageIndices);
        
        
        // Alien
        alienMoveDownAnimationType = new AdvancedAnimation[4];
        alienMoveLeftAnimationType = new AdvancedAnimation[4];
        alienMoveRightAnimationType = new AdvancedAnimation[4];
        alienMoveUpAnimationType = new AdvancedAnimation[4];
        alienStayAnimationType = new AdvancedAnimation[4];
        alienDeadAnimationType = new AdvancedAnimation[4];

        for (int i = 1; i <= 4; i++) {
            String str = "./data/alienType" + ((Integer)i).toString();

            ArrayList<Integer> alienType1MoveDownImageIndices = new ArrayList<>();
                alienType1MoveDownImageIndices.add(imageAddresses.indexOf(str + "MoveDown1.png"));
                alienType1MoveDownImageIndices.add(imageAddresses.indexOf(str + "MoveDown2.png"));
            alienMoveDownAnimationType[i-1] = new AdvancedAnimation(100, alienType1MoveDownImageIndices);

            ArrayList<Integer> alienType1MoveLeftImageIndices = new ArrayList<>();
                alienType1MoveLeftImageIndices.add(imageAddresses.indexOf(str + "MoveLeft1.png"));
                alienType1MoveLeftImageIndices.add(imageAddresses.indexOf(str + "MoveLeft2.png"));
                alienType1MoveLeftImageIndices.add(imageAddresses.indexOf(str + "MoveLeft3.png"));
            alienMoveLeftAnimationType[i-1] = new AdvancedAnimation(100, alienType1MoveLeftImageIndices);

            ArrayList<Integer> alienType1MoveRightImageIndices = new ArrayList<>();
                alienType1MoveRightImageIndices.add(imageAddresses.indexOf(str + "MoveRight1.png"));
                alienType1MoveRightImageIndices.add(imageAddresses.indexOf(str + "MoveRight2.png"));
                alienType1MoveRightImageIndices.add(imageAddresses.indexOf(str + "MoveRight3.png"));
            alienMoveRightAnimationType[i-1] = new AdvancedAnimation(100, alienType1MoveRightImageIndices);

            ArrayList<Integer> alienType1MoveUpImageIndices = new ArrayList<>();
                alienType1MoveUpImageIndices.add(imageAddresses.indexOf(str + "MoveUp1.png"));
                alienType1MoveUpImageIndices.add(imageAddresses.indexOf(str + "MoveUp2.png"));
            alienMoveUpAnimationType[i-1] = new AdvancedAnimation(100, alienType1MoveUpImageIndices);

            ArrayList<Integer> alienType1StayImageIndices = new ArrayList<>();
                alienType1StayImageIndices.add(imageAddresses.indexOf(str + "Stay1.png"));
                alienType1StayImageIndices.add(imageAddresses.indexOf(str + "Stay2.png"));
            alienStayAnimationType[i-1] = new AdvancedAnimation(100, alienType1StayImageIndices);

            ArrayList<Integer> alienType1DeadImageIndices = new ArrayList<>();
                alienType1DeadImageIndices.add(imageAddresses.indexOf(str + "Dying1.png"));
                alienType1DeadImageIndices.add(imageAddresses.indexOf(str + "Dying2.png"));
                alienType1DeadImageIndices.add(imageAddresses.indexOf(str + "Dying3.png"));
            alienDeadAnimationType[i-1] = new AdvancedAnimation(500, alienType1DeadImageIndices);
        }


        // Power ups
        ArrayList<Integer> powerUpControlBombsImageIndices = new ArrayList<>();
            powerUpControlBombsImageIndices.add(imageAddresses.indexOf("./data/powerUpControlBombs.png"));
        powerUpControlBombsAnimation = new AdvancedAnimation(100, powerUpControlBombsImageIndices);

        ArrayList<Integer> powerUpIncreaseBombsImageIndices = new ArrayList<>();
            powerUpIncreaseBombsImageIndices.add(imageAddresses.indexOf("./data/powerUpIncreaseBombs.png"));
        powerUpIncreaseBombsAnimation = new AdvancedAnimation(100, powerUpIncreaseBombsImageIndices);

        ArrayList<Integer> powerUpIncreasePointsImageIndices = new ArrayList<>();
            powerUpIncreasePointsImageIndices.add(imageAddresses.indexOf("./data/powerUpIncreasePoints.png"));
        powerUpIncreasePointsAnimation = new AdvancedAnimation(100, powerUpIncreasePointsImageIndices);

        ArrayList<Integer> powerUpIncreaseRadiusImageIndices = new ArrayList<>();
            powerUpIncreaseRadiusImageIndices.add(imageAddresses.indexOf("./data/powerUpIncreaseRadius.png"));
        powerUpIncreaseRadiusAnimation = new AdvancedAnimation(100, powerUpIncreaseRadiusImageIndices);

        ArrayList<Integer> powerUpIncreaseSpeedImageIndices = new ArrayList<>();
            powerUpIncreaseSpeedImageIndices.add(imageAddresses.indexOf("./data/powerUpIncreaseSpeed.png"));
        powerUpIncreaseSpeedAnimation = new AdvancedAnimation(100, powerUpIncreaseSpeedImageIndices);

        // decreasing power-ups
        ArrayList<Integer> powerUpDecreaseBombsImageIndices = new ArrayList<>();
            powerUpDecreaseBombsImageIndices.add(imageAddresses.indexOf("./data/powerUpDecreaseBombs.png"));
        powerUpDecreaseBombsAnimation = new AdvancedAnimation(100, powerUpDecreaseBombsImageIndices);

        ArrayList<Integer> powerUpDecreasePointsImageIndices = new ArrayList<>();
            powerUpDecreasePointsImageIndices.add(imageAddresses.indexOf("./data/powerUpDecreasePoints.png"));
        powerUpDecreasePointsAnimation = new AdvancedAnimation(100, powerUpDecreasePointsImageIndices);

        ArrayList<Integer> powerUpDecreaseRadiusImageIndices = new ArrayList<>();
            powerUpDecreaseRadiusImageIndices.add(imageAddresses.indexOf("./data/powerUpDecreaseRadius.png"));
        powerUpDecreaseRadiusAnimation = new AdvancedAnimation(100, powerUpDecreaseRadiusImageIndices);

        ArrayList<Integer> powerUpDecreaseSpeedImageIndices = new ArrayList<>();
            powerUpDecreaseSpeedImageIndices.add(imageAddresses.indexOf("./data/powerUpDecreaseSpeed.png"));
        powerUpDecreaseSpeedAnimation = new AdvancedAnimation(100, powerUpDecreaseSpeedImageIndices);
    }




    private void loadEmptyBlockAppropriateAnimation(int i, int j){
        EmptyBlock emptyBlock = (EmptyBlock) map.getBlock(i, j);
        if (emptyBlock.getStatus() == EmptyBlockStatus.normal) {
            emptyBlock.setAnimation(emptyBlockAnimation);
            return;
        }
        if (emptyBlock.getDelayToExplosionCenter() < 0)   // loaded before
            return;

        if (emptyBlock.getStatus() == EmptyBlockStatus.normalExplosion){
            if (emptyBlock.getDelayToExplosionCenter() <= 0)
                emptyBlock.setAnimation(normalExplosionAnimation);
            else emptyBlock.setAnimation(emptyBlockAnimation);
        }
        else if (emptyBlock.getStatus() == EmptyBlockStatus.downEdgeExplosion){
            if (emptyBlock.getDelayToExplosionCenter() <= 0)
                emptyBlock.setAnimation(downEdgeExplosionAnimation);
            else emptyBlock.setAnimation(emptyBlockAnimation);
        }
        else if (emptyBlock.getStatus() == EmptyBlockStatus.upEdgeExplosion){
            if (emptyBlock.getDelayToExplosionCenter() <= 0) 
                emptyBlock.setAnimation(upEdgeExplosionAnimation);
            else emptyBlock.setAnimation(emptyBlockAnimation);
        }
        else if (emptyBlock.getStatus() == EmptyBlockStatus.leftEdgeExplosion){
            if (emptyBlock.getDelayToExplosionCenter() <= 0) 
                emptyBlock.setAnimation(leftEdgeExplosionAnimation);
            else emptyBlock.setAnimation(emptyBlockAnimation);
        }
        else if (emptyBlock.getStatus() == EmptyBlockStatus.rightEdgeExplosion){
            if (emptyBlock.getDelayToExplosionCenter() <= 0) 
                emptyBlock.setAnimation(rightEdgeExplosionAnimation);
            else emptyBlock.setAnimation(emptyBlockAnimation);
        }

        emptyBlock.setDelayToExplosionCenter(emptyBlock.getDelayToExplosionCenter() - animationThreadTime);
    }





    private void loadAlienAppropriateAnimation(Alien alien) {
        if (alien.getStatus() == AlienStatus.MovingDown) {
            alien.setAnimation(alienMoveDownAnimationType[alien.getType() - 1]);  // - 1 : to make zero-based
        }
        else if (alien.getStatus() == AlienStatus.MovingUp) {
            alien.setAnimation(alienMoveUpAnimationType[alien.getType() - 1]);
        }
        else if (alien.getStatus() == AlienStatus.MovingLeft) {
            alien.setAnimation(alienMoveLeftAnimationType[alien.getType() - 1]);
        }
        else if (alien.getStatus() == AlienStatus.MovingRight) {
            alien.setAnimation(alienMoveRightAnimationType[alien.getType() - 1]);
        }
        else if (alien.getStatus() == AlienStatus.Stay) {
            alien.setAnimation(alienStayAnimationType[alien.getType() - 1]);
        }
        else if (alien.getStatus() == AlienStatus.Dead) {
            alien.setAnimation(alienDeadAnimationType[alien.getType() - 1]);
        }
    }




    private void reloadImages() {
        powerImages = new ArrayList<>();
        for (int i = map.getNumberOfPowers() - 1; i >= 0; i--) {   // backward loop
            PowerUp power = map.getPower(i);
            DisplayImage powerImage = new DisplayImage(new Point(power.location.getY() * cellSize, power.location.getX() * cellSize));
            powerImage.setImage(allImages[power.animation.getCurrentImageIndex()]);
            powerImages.add(powerImage);
        }


        for (int i = 0; i < map.getNumberOfRows(); i++)
            for (int j = 0; j < map.getNumberOfCols(); j++) {
                blockImages[i][j].setImage(allImages[map.getBlock(i, j).animation.getCurrentImageIndex()]);
            }

        for (int i = 0; i < map.getNumberOfPlayers(); i++) {
            if (map.getPlayer(i).getPlayerStatus() == PlayerStatus.Dead)
                continue;
            playerImages[i].setImage(allImages[map.getPlayer(i).animation.getCurrentImageIndex()]);
        }

        alienImages = new ArrayList<>();
        for (int i = 0; i < map.getNumberOfAliens(); i++) {
            Alien alien = map.getAlien(i);
            DisplayImage alienImage = new DisplayImage(new Point(alien.location.getY() * cellSize, alien.location.getX() * cellSize));
            alienImage.setImage(allImages[alien.animation.getCurrentImageIndex()]);
            alienImages.add(alienImage);
        }

        bombImages = new ArrayList<>();
        for (int i = 0; i < map.getNumberOfPlayers(); i++) {
            for (int j = 0; j < map.getPlayer(i).getNumberOfPlantedBombs(); j++) {
                Bomb bomb = map.getPlayer(i).getBomb(j);
                if (bomb.getExplosionTime() > 0) {
                    // +5 below here, so that the player's head could be seen
                    DisplayImage bombImage = new DisplayImage(new Point(bomb.location.getY() * cellSize + 5, bomb.location.getX() * cellSize + 5));
                    bombImage.setImage(allImages[bomb.animation.getCurrentImageIndex()]);
                    bombImages.add(bombImage);
                }
            }
        }
    }


    // Inner class
    public class DisplayImage{
        Point location;
        Image image;

        public DisplayImage(Point location){
            this.location = location;
        }
        public void setImage(Image image){ this.image = image; }
        public void setLocation(Point location){  this.location = location; }

        public void renderImage(Graphics2D g, int cellWidth, int cellHeight) {
            g.drawImage(image, location.getX(), location.getY(), cellWidth, cellHeight, (ImageObserver) PaintPanel.this);
        }
    }



    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // painting order is important
        for (int i = map.getNumberOfPowers() - 1; i >= 0; i--) {
            if (! map.getPower(i).isVisible())
                continue;
            powerImages.get(i).renderImage((Graphics2D) g, cellSize, cellSize);
        }

        for (int i = 0; i < map.getNumberOfRows(); i++)
            for (int j = 0; j < map.getNumberOfCols(); j++) {
                if (map.getBlock(i, j) instanceof EmptyBlock){
                    if (((EmptyBlock) map.getBlock(i, j)).hasPowerUp() && ((EmptyBlock) map.getBlock(i, j)).getStatus() == EmptyBlockStatus.normal)
                        continue;
                }
                blockImages[i][j].renderImage((Graphics2D) g, cellSize, cellSize);
            }

        for (int i = 0; i < map.getNumberOfPlayers(); i++) {
            if (map.getPlayer(i).getPlayerStatus() == PlayerStatus.Dead)
                continue;
            // Note: getX, getY must be substituted because
            playerImages[i].setLocation(new Point(map.getPlayer(i).location.getY() * cellSize, map.getPlayer(i).location.getX() * cellSize));
            playerImages[i].renderImage((Graphics2D) g, cellSize, cellSize);
        }
        for (DisplayImage bombImage: bombImages)
            bombImage.renderImage((Graphics2D) g, cellSize - 5, cellSize - 5);  // smaller so that the player could be seen
        for (DisplayImage alienImage: alienImages)
            alienImage.renderImage((Graphics2D) g, cellSize, cellSize);
    }


    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        if (map.getPlayer(0).getPlayerStatus() == PlayerStatus.Dead)
            return;

        if (keyboardDate == null)
            keyboardDate = new Date();
        else if (new Date().getTime() - keyboardDate.getTime() > map.getPlayer(0).getKeyRespondTime()){
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                map.getPlayer(0).move(Direction.Right);
                map.getPlayer(0).setAnimation(playerMoveRightAnimation); // change
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                map.getPlayer(0).move(Direction.Left);
                map.getPlayer(0).setAnimation(playerMoveLeftAnimation);  // change
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                map.getPlayer(0).move(Direction.Down);
                map.getPlayer(0).setAnimation(playerMoveDownAnimation); // change
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                map.getPlayer(0).move(Direction.Up);
                map.getPlayer(0).setAnimation(playerMoveUpAnimation);   // change
            }
            keyboardDate = new Date();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        try {
            map.semaphore.acquire();
        } catch (InterruptedException e1) { e1.printStackTrace(); }

        if (map.getPlayer(0).getPlayerStatus() == PlayerStatus.Dead)
            return;

        if (e.getKeyCode() == KeyEvent.VK_B && e.isAltDown()){
            map.getPlayer(0).explodeFirstBomb();
        }
        else if (e.getKeyCode() == KeyEvent.VK_B) {
            map.planBombInMap(0);
        }
        keyboardDate = null;
        map.getPlayer(0).stay();
        map.getPlayer(0).setAnimation(playerStayAnimation);

        map.semaphore.release();
    }




    public Map getMap() {
        synchronized (this){
            return map;
        }
    }

    public void setMap(Map map) {
        synchronized (this){
            this.map = map;
            initialize();
        }
    }

    public void stopThreads(){
        synchronized (this) {
            animationThread.stop();
            map.stopThreads();
        }
    }
}
