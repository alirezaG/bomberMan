package gamePackage;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Alien extends Block implements Animatable {
    private final Map map;
    private AlienStatus status, previousStatus;
    private final int delayTimeToMove;
    private int remainingTimeToMove;
    private int remainingTimeToDisappear;
    private final int type;


    Alien(int x, int y, Map map, int type) {
        super(x, y);
        this.map = map;
        this.type = type;
        this.status = AlienStatus.Stay;
        this.previousStatus = AlienStatus.Undefined;

        switch (type){
            case 1:
                delayTimeToMove = map.defaultKeyRespondTime * 2;      break;
            case 2:
                delayTimeToMove = map.defaultKeyRespondTime;          break;
            case 3:
                delayTimeToMove = map.defaultKeyRespondTime / 2 + 50; break;
            default:
                delayTimeToMove = map.defaultKeyRespondTime / 2 + 50; break;
        }
        remainingTimeToMove = delayTimeToMove;
    }


    Alien(BufferedReader reader, Map map) throws IOException {
        super(0,0);  // temp
        this.map = map;
        String line = reader.readLine();
        ArrayList<Integer> characteristics = parseAlien(line);

        this.location.setX(characteristics.get(0));
        this.location.setY(characteristics.get(1));
        this.type = characteristics.get(2);
        this.status = AlienStatus.getStatus(characteristics.get(3));
        this.previousStatus = AlienStatus.getStatus(characteristics.get(4));
        this.delayTimeToMove = characteristics.get(5);
        this.remainingTimeToMove = characteristics.get(6);
        this.remainingTimeToDisappear = characteristics.get(7);
    }


    @Override
    public void step() { animation.animate(); }

    @Override
    public void save(BufferedWriter writer) {
        String str = "Alien" + "#" + this.location.getX() + "#" + this.location.getY() + "#" + this.type + "#"
                + this.status + "#" + this.previousStatus + "#" + this.delayTimeToMove + "#" + this.remainingTimeToMove
                + "#" + this.remainingTimeToDisappear + "#\n";
        try {
            writer.write(str);
        } catch (IOException e) { e.printStackTrace(); }
    }


    private ArrayList<Integer> parseAlien(String str){
        ArrayList<Integer> characteristics = new ArrayList<>();

        return characteristics;
    }




    private void move(Direction direction){
        int dx = 0, dy = 0;
        if (direction == Direction.Left) {
            dy = -1;
            setStatus(AlienStatus.MovingLeft);
        }
        else if (direction == Direction.Right) {
            dy = +1;
            setStatus(AlienStatus.MovingRight);
        }
        else if (direction == Direction.Up) {
            dx = -1;
            setStatus(AlienStatus.MovingUp);
        }
        else if (direction == Direction.Down) {
            dx = +1;
            setStatus(AlienStatus.MovingDown);
        }
        int destX = this.location.getX() + dx, destY = this.location.getY() + dy;

        if (! canMoveTo(destX, destY))
            return;
        this.setLocation(new Point(destX, destY));
    }


    private boolean canMoveTo(int destX, int destY) {
        if (destX <= 0 || destY <= 0 || destX >= map.getNumberOfRows() || destY >= map.getNumberOfCols())
            return false;
        if (map.getBlock(destX, destY) instanceof EmptyBlock)
            if (((EmptyBlock) map.getBlock(destX, destY)).hasBomb())
                return false;
        if (type != 4 && map.getBlock(destX, destY) instanceof Stone || map.getBlock(destX, destY) instanceof Wall)
            return false;
        return true;
    }



    private void move() {
        Direction bestDir = calcBestDirection();
        move(bestDir);
    }



    private Direction calcBestDirection(){
        Direction direction = Direction.Undefined;
                       // System.out.println("animation thread slept!");

        if (type == 1) {
            Random random = new Random();
            int dx, dy;
            do {
                int rnd = random.nextInt(4);
                dx = dy = 0;
                switch (rnd) {
                    case 0:
                        direction = Direction.Up;
                        dx = -1;
                        break;
                    case 1:
                        direction = Direction.Right;
                        dy = +1;
                        break;
                    case 2:
                        direction = Direction.Down;
                        dx = +1;
                        break;
                    default:
                        direction = Direction.Left;
                        dy = -1;
                        break;
                }
            } while (! canMoveTo(location.getX() + dx, location.getY() + dy)) ;
        }
        else {
            int minDist = 10000;  // inf
            Player player = map.getPlayer(0);
            // Up
            int dist = Math.abs(player.location.getX() - (this.location.getX() - 1)) + Math.abs(player.location.getY() - this.location.getY());
            if (dist < minDist && canMoveTo(location.getX() - 1, location.getY())) {
                minDist = dist;
                direction = Direction.Up;
            }

            // Down
            dist = Math.abs(player.location.getX() - (this.location.getX() + 1)) + Math.abs(player.location.getY() - this.location.getY());
            if (dist < minDist && canMoveTo(location.getX() + 1, location.getY())) {
                minDist = dist;
                direction = Direction.Down;
            }

            // Right
            dist = Math.abs(player.location.getX() - (this.location.getX())) + Math.abs(player.location.getY() - (this.location.getY() + 1));
            if (dist < minDist && canMoveTo(location.getX(), location.getY() + 1)) {
                minDist = dist;
                direction = Direction.Right;
            }

            // Left
            dist = Math.abs(player.location.getX() - (this.location.getX())) + Math.abs(player.location.getY() - (this.location.getY() - 1));
            if (dist < minDist && canMoveTo(location.getX(), location.getY() - 1)) {
                minDist = dist;
                direction = Direction.Left;
            }
        }
        return direction;
    }


    public void decreaseDelayTime(int d){
        remainingTimeToMove -= d;
        if (status == AlienStatus.Dead)
            remainingTimeToDisappear -= d;

        if (status == AlienStatus.Dead && remainingTimeToDisappear <= 0)
            setStatus(AlienStatus.Disappeared);

        if (status != AlienStatus.Dead && status != AlienStatus.Disappeared && remainingTimeToMove <= 0){
            move();
            remainingTimeToMove = delayTimeToMove; // reset
        }
    }



    public void die(){
        status = AlienStatus.Dead;
        remainingTimeToDisappear = 1500;
    }


    // Setters and Getters
    public void setLocation(Point location){
        synchronized (this) {
            this.location = location;
        }
    }

    private void setStatus(AlienStatus status){
        synchronized (this){
            previousStatus = this.status;
            this.status = status;
        }
    }

    public AlienStatus getStatus(){
        synchronized (this){
            return status;
        }
    }

    public AlienStatus getPreviousStatus(){
        synchronized (this){
            return previousStatus;
        }
    }

    public int getType(){
        return type;
    }

}
