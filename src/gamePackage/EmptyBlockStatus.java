package gamePackage;

public enum EmptyBlockStatus {
    normal, upEdgeExplosion, downEdgeExplosion, rightEdgeExplosion, leftEdgeExplosion, normalExplosion
}
