package gamePackage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public abstract class Animation implements Animatable {
    protected boolean status = true;
    protected Date nextTime;
    protected int stepDelay;

    public boolean isAnimating(){ return status; }
    public void setAnimating(boolean status){ this.status = status; }

    public int getStepDelay(){ return stepDelay; }
    public void setStepDelay(int stepDelay) { this.stepDelay = stepDelay; }

    Animation(int stepDelay){
        this.stepDelay = stepDelay;
        nextTime = new Date();   // the first animation should be shown at the moment
    }


    void animate(){
        if (status){
            if (nextTime.before(new Date() /*NOW*/)){
                step();
                nextTime.setTime(new Date().getTime() + stepDelay);
            }
        }
    }
}
