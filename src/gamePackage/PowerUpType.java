package gamePackage;

public enum PowerUpType {
    IncreaseBombs, IncreaseSpeed, IncreasePoints, IncreaseRadius, ControlBomb,
    DecreaseBombs, DecreaseSpeed, DecreasePoints, DecreaseRadius
}
