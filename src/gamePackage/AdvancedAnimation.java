package gamePackage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AdvancedAnimation extends Animation implements Animatable{
    private ArrayList<Integer> imageIndices;
    private int pointer;

    public AdvancedAnimation(int stepDelay, ArrayList<Integer> imageIndices) {
        super(stepDelay);
        this.imageIndices = imageIndices;
        pointer = imageIndices.size() - 1; // because we call step() once before getCurrentImageIndex()
    }

    public int getCurrentImageIndex(){
        return imageIndices.get(pointer);
    }

    @Override
    public void step() {
        pointer++;
        pointer %= imageIndices.size();
    }
}
