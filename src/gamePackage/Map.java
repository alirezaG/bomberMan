package gamePackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Map {
    private final int numberOfRows, numberOfCols, numberOfPlayers;
    private boolean isPaused, finished = false;
    private int level;

    private Block[][] block;
    private ArrayList<PowerUp> powers;
    private Player[] players;
    private ArrayList<Bomb> bombs;

    private ArrayList<Alien> aliens;

    private Thread thread;
    private final int threadTime = 100;

    public final int delayTimeForAdjacentBombExplosion = 80;
    public final int defaultKeyRespondTime = 600, defaultMinimumKeyRespondTime = 130, defaultMaximumKeyRespondTime = 600;

    static Semaphore semaphore = new Semaphore(1);


    Map(BufferedReader reader) throws IOException {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) { e.printStackTrace(); }

        this.numberOfRows = Integer.parseInt(reader.readLine());
        this.numberOfCols = Integer.parseInt(reader.readLine());
        this.numberOfPlayers = Integer.parseInt(reader.readLine());

        int numberOfAliens = Integer.parseInt(reader.readLine());
        loadAliens(reader, numberOfAliens);
        loadBlocks(reader, numberOfRows, numberOfCols);

        runThread();

        semaphore.release();
    }



    Map(int numberOfRows, int numberOfCols, int numberOfPlayers, int numberOfAliens) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) { e.printStackTrace(); }

        this.numberOfRows = numberOfRows;
        this.numberOfCols = numberOfCols;
        this.numberOfPlayers = numberOfPlayers;
        this.level = 1;
        initializeStandardMap(numberOfAliens);
        runThread();

        semaphore.release();
    }

    private void runThread(){
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    if (getIsPaused()){
                        continue;
                    }

                    try {
                        semaphore.acquire();
                    } catch (InterruptedException e) { e.printStackTrace(); }

                    for (int i = numberOfPlayers - 1; i >= 0; i--) {
                        // no need to check playerStatus == Dead because his bombs can explode after his death
                        for (int j = 0; j < players[i].getNumberOfPlantedBombs(); j++) {
                            Bomb bomb = players[i].getBomb(j);
                            bomb.decreaseTime(threadTime);   // explosion will happen inside this function if the right time comes
                        }
                        for (int j = players[i].getNumberOfPlantedBombs()-1; j >= 0; j--) {     // backward loop
                            if (players[i].getBomb(j).getStatus() == BombStatus.Burning && players[i].getBomb(j).getBurningTime() <= 0) {
                                players[i].getBomb(j).finish();
                                players[i].removeBomb(j);
                            }
                        }
                    }

                    for (int i = getNumberOfAliens() - 1; i >= 0; i--){
                        Alien alien = getAlien(i);
                        if (alien.getStatus() == AlienStatus.Disappeared) {
                            aliens.remove(i);
                            continue;
                        }
                        alien.decreaseDelayTime(threadTime);  // movement happens inside this function if the right time comes
                    }

                    for (int i = numberOfPlayers - 1; i >= 0; i--) {
                        if (players[i].getPlayerStatus() == PlayerStatus.Dead)
                            continue;
                        EmptyBlock playerBlock = (EmptyBlock) getBlock(players[i].location.getX(), players[i].location.getY());
                        if (playerBlock.hasAlien()) {
                            killPlayer(i);
                            finished = true;
                        }
                    }


                    for (int i = numberOfPlayers - 1; i >= 0; i--) {
                        if (players[i].getPlayerStatus() == PlayerStatus.Dead)
                            continue;
                        for (int j = getNumberOfPowers() - 1; j >= 0; j--){
                            PowerUp power = getPower(j);
                            if (players[i].location.equal(power.location)) {
                                players[i].eatPowerUp(power);
                                removePowerUp(j);
                            }
                        }
                    }

                    semaphore.release();

                    try{
                        Thread.sleep(threadTime);
                    } catch (InterruptedException e) { e.printStackTrace(); }
                }
            }
        });
        thread.start();
    }



    private void initializeStandardMap(int numberOfAliens){
        Random rng = new Random();
        block = new Block[numberOfRows][numberOfCols];
        players = new Player[numberOfPlayers];

        for (int i = 0; i < numberOfPlayers; i++) {  // now we have only one player
            players[i] = new Player(1, 1, this, 5000, 5, defaultKeyRespondTime, 0, 1,
                 0, false);
        }

        bombs = new ArrayList<>();
        aliens = new ArrayList<>();
        powers = new ArrayList<>();

        // Generating blocks
        ArrayList<Point> spawnPoints = new ArrayList<>();

        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfCols; j++) {
                if (i == 0 || j == 0 || i == numberOfRows-1 || j == numberOfCols-1)
                    block[i][j] = new Stone(i, j);
                else if (i % 2 == 0 && j % 2 == 0)
                    block[i][j] = new Stone(i, j);
                else if (i <= 5 && j <= 5)
                    block[i][j] = new EmptyBlock(i, j, this);   // no aliens in this area
                else {
                    int tmp = rng.nextInt(40);
                    if (tmp % 4 == 0) {
                        block[i][j] = new Wall(i, j);
                        if (tmp % 12 == 0) {
                            PowerUp power = generatePowerUp(new Point(i, j), rng.nextInt(9));
                            powers.add(power);
                        }
                    }
                    else{
                        block[i][j] = new EmptyBlock(i, j, this);
                      //  PowerUp power = generatePowerUp(new Point(i, j), rng.nextInt(9));
                      //  powers.add(power);
                        spawnPoints.add(new Point(i, j));
                    }
                }
            }

        // Generating aliens
        // type 2, 3, 4
        for (int type = 2; type <= 4; type++) {
            for (int i = 0; i < (numberOfAliens / 10) * (5 - type); i++) {
                int randomIndex = rng.nextInt(spawnPoints.size());
                Alien alien = new Alien(spawnPoints.get(randomIndex).getX(), spawnPoints.get(randomIndex).getY(), this, type);
                spawnPoints.remove(randomIndex);
                aliens.add(alien);
            }
        }
        // type 1
        while (aliens.size() != numberOfAliens){
            int randomIndex = rng.nextInt(spawnPoints.size());
            Alien alien = new Alien(spawnPoints.get(randomIndex).getX(), spawnPoints.get(randomIndex).getY(), this, 1);
            spawnPoints.remove(randomIndex);
            aliens.add(alien);
        }
    }


    private PowerUp generatePowerUp(Point location, int rnd){
        PowerUp power;
        switch(rnd){
            case 0:
                power = new PowerUp(location, this, PowerUpType.IncreasePoints);
                break;
            case 1:
                power = new PowerUp(location, this, PowerUpType.IncreaseSpeed);
                break;
            case 2:
                power = new PowerUp(location, this, PowerUpType.IncreaseBombs);
                break;
            case 3:
                power = new PowerUp(location, this, PowerUpType.IncreaseRadius);
                break;
            case 4:
                power = new PowerUp(location, this, PowerUpType.DecreasePoints);
                break;
            case 5:
                power = new PowerUp(location, this, PowerUpType.DecreaseSpeed);
                break;
            case 6:
                power = new PowerUp(location, this, PowerUpType.DecreaseBombs);
                break;
            case 7:
                power = new PowerUp(location, this, PowerUpType.DecreaseRadius);
                break;
            default:
                power = new PowerUp(location, this, PowerUpType.ControlBomb);
                break;
        }
        return power;
    }


    // Functions for Bombs
    public void planBombInMap(int playerIndex){
        Player bomber = getPlayer(playerIndex);
        Bomb bomb = new Bomb(bomber.location.getX(), bomber.location.getY(), this, bomber, bomber.getBombExplosionTime(), bomber.getBombExplosionRange());
        bomber.plantBomb(bomb);
    }



    public void killPlayer(int index){
        synchronized (this){
            Player player = getPlayer(index);
            player.die();
        }
    }


    public void killAlien(int index){
        synchronized (this){
            Alien alien = getAlien(index);
            alien.die();
        }
    }

    public void removePowerUp(int index){
        synchronized (this){
            powers.remove(index);
        }
    }


    // Setters and Getters
    public boolean getIsPaused() {
        synchronized (this){
            return isPaused;
        }
    }

    public void setIsPaused(boolean bool){
        synchronized (this){
            isPaused = bool;
        }
    }

    public Player getPlayer(int index){
        synchronized (this){
            return players[index];
        }
    }

    public int getNumberOfRows(){
        synchronized (this){
            return this.numberOfRows;
        }
    }

    public int getNumberOfCols(){
        synchronized (this){
            return this.numberOfCols;
        }
    }

    public int getNumberOfPlayers(){
        synchronized (this){
            return this.numberOfPlayers;
        }
    }

    public Block getBlock(int i, int j){
        synchronized (this){
            return block[i][j];
        }
    }

    public void setBlock(int i, int j, Block newBlock){
        synchronized (this){
            block[i][j] = newBlock;
        }
    }

    public int getDelayTimeForAdjacentBombExplosion(){
        synchronized (this){
            return delayTimeForAdjacentBombExplosion;
        }
    }

    public int getNumberOfAliens(){
        synchronized (this){
            return aliens.size();
        }
    }

    public Alien getAlien(int index){
        synchronized (this){
            return aliens.get(index);
        }
    }

    public int getNumberOfPowers(){
        synchronized (this){
            return powers.size();
        }
    }

    public PowerUp getPower(int index){
        synchronized (this){
            return powers.get(index);
        }
    }

    public void stopThreads(){
        synchronized (this){
            thread.stop();
        }
    }

    public int getLevel(){
        synchronized (this){
            return level;
        }
    }

    public boolean isFinished() {
        synchronized (this){
            return finished;
        }
    }

    // Saving and Loading functions
    public void saveAll(BufferedWriter writer){
        try {
            writer.write(numberOfRows + "\n");
            writer.write(numberOfCols + "\n");
            writer.write(numberOfPlayers + "\n");
            writer.write(getNumberOfAliens() + "\n");

            for (Alien alien: aliens){
                alien.save(writer);
            }
        }
        catch (IOException e) { e.printStackTrace(); }
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfCols; j++)
                block[i][j].save(writer);
        for (int i = 0; i < powers.size(); i++)
            powers.get(i).save(writer);
        for (int i = 0; i < numberOfPlayers; i++)
            players[i].save(writer);
        for (Bomb bomb: bombs)
            bomb.save(writer);
        try {
            writer.write("END");
        } catch (IOException e) { e.printStackTrace(); }
    }


    private void loadAliens(BufferedReader reader, int numberOfAliens){
        aliens = new ArrayList<>();
        for (int i = 0; i < numberOfAliens; i++) {
            Alien alien = null;
            try {
                alien = new Alien(reader, this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            aliens.add(alien);
        }
    }

    private void loadBlocks(BufferedReader reader, int numberOfRows, int numberOfCols){

    }

    private void loadAll(BufferedReader reader){    // load must be changed --> numberOfPlantedBombs
        powers = new ArrayList<>();
        block = new Block[numberOfRows][numberOfCols];
        players = new Player[numberOfPlayers];
        bombs = new ArrayList<>();

        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j < numberOfCols; j++){
                String line = "";
                try {
                    line = reader.readLine();
                } catch (IOException e) { e.printStackTrace(); }

                GameEnum type = parseType(line);
                if (type == GameEnum.EmptyBlock)
                    block[i][j] = new EmptyBlock(i, j, this);
                else if (type == GameEnum.Wall)
                    block[i][j] = new Wall(i, j);
                else if (type == GameEnum.Stone)
                    block[i][j] = new Stone(i, j);
            }
        // TO DO items and Bombs

        /************/
        for (int i = 0; i < numberOfPlayers; i++) {
            String line = "";
            try {
                line = reader.readLine();
            } catch (IOException e) { e.printStackTrace(); }

            ArrayList<Integer> traits = parsePlayer(line);
            // change it
            players[i] = new Player(traits.get(0), traits.get(1), this, traits.get(2), traits.get(3), traits.get(5), 0, 1,
                    0, false);
        }
    }


    private GameEnum parseType(String str){
        GameEnum type;
        if (str.charAt(0) == 'E')
            type = GameEnum.EmptyBlock;
        else if (str.charAt(0) == 'W')
            type = GameEnum.Wall;
        else if (str.charAt(0) == 'S')
            type = GameEnum.Stone;
        else type = GameEnum.Undefined;
        return type;
    }

    private ArrayList<Integer> parsePlayer(String str){
        ArrayList<Integer> traits = new ArrayList<>();
        int i = 0;
        while (str.charAt(i) != '#') i++;
        int j = i+1;
        int locX = 0;
        while (str.charAt(j) != '#') {
            locX = locX * 10 + (str.charAt(j) - '0');
            j++;
        }
        i = j; j++;
        int locY = 0;
        while (str.charAt(j) != '#') {
            locY = locY * 10 + (str.charAt(j) - '0');
            j++;
        }

        i = j; j++;
        int expTime = 0;
        while (str.charAt(j) != '#') {
            expTime = expTime * 10 + (str.charAt(j) - '0');
            j++;
        }

        i = j; j++;
        int expRange = 0;
        while (str.charAt(j) != '#') {
            expRange = expRange * 10 + (str.charAt(j) - '0');
            j++;
        }

        i = j; j++;
        int bombPlanted = 0;
        if (str.charAt(j+1) == 't') {// true
            bombPlanted = 1;
            j += 4;
        } else  j += 5;

        i = j; j++;
        int keyRespond = 0;
        while (str.charAt(j) != '#') {
            keyRespond = keyRespond * 10 + (str.charAt(j) - '0');
            j++;
        }
        traits.add((Integer)locX);
        traits.add((Integer)locY);
        traits.add((Integer)expTime);
        traits.add((Integer)expRange);
        traits.add((Integer)bombPlanted);
        traits.add((Integer)keyRespond);
        return traits;
    }
}




