package gamePackage;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class StartFrame extends JFrame {
    private JPanel contentPane;
    private JPanel btnPanel;
    private JButton newGameBtn, loadGameBtn, exitGameBtn;
    private boolean gameStarted = false;

    StartFrame(){
        initialize();
    }

    private void initialize(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPane = (JPanel) this.getContentPane();
        this.setSize(new Dimension(450, 200));
        this.setTitle("Bomber Man");
        ImageIcon iconImg = new ImageIcon("./data/bomberManWithShield2.jpg");
        this.setIconImage(iconImg.getImage());
        StartFrame thisFrame = this;

        contentPane.setLayout(new BorderLayout());
        btnPanel = new JPanel(new GridLayout(6, 1));
        newGameBtn = new JButton("New Game");


        newGameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!gameStarted) {
                    NewGameAskFrame newGameAskFrame = new NewGameAskFrame();

                    newGameAskFrame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                            if (newGameAskFrame.getNumberOfCols() > 0 && newGameAskFrame.getNumberOfRows() > 0) {
                                gameStarted = true;
                                GameFrame gameFrame = new GameFrame(thisFrame, newGameAskFrame.getNumberOfRows(), newGameAskFrame.getNumberOfCols(),
                                        1, newGameAskFrame.getNumberOfAliens());
                            }
                        }
                    });
                } else {
                    JOptionPane.showMessageDialog(contentPane, "The Game Has Already Started!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });


        loadGameBtn = new JButton("Load Game");
        loadGameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Open your file to load the game");
                FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Text Files","txt");
                fileChooser.addChoosableFileFilter(fileFilter);
                int userSelection = fileChooser.showOpenDialog(thisFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File openedFile = fileChooser.getSelectedFile();
                    BufferedReader reader = null;
                    try {
                        reader = new BufferedReader(new FileReader(openedFile));
                        gameStarted = true;GameFrame
                        gameFrame = new GameFrame(thisFrame, reader);
                    } catch (FileNotFoundException e1) { e1.printStackTrace(); }
                    finally {
                        try { reader.close(); } catch (IOException e1) { e1.printStackTrace(); }
                    }
                }
            }
        });


        exitGameBtn = new JButton("Exit Game");
        exitGameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes", "No"};
                int ret = JOptionPane.showOptionDialog(contentPane, "Are you sure?", null,
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, iconImg, options, options[1]);
                if (ret == 0) System.exit(0);
            }
        });

        btnPanel.add(newGameBtn);
        btnPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        btnPanel.add(loadGameBtn);
        btnPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        btnPanel.add(exitGameBtn);

        contentPane.add(btnPanel, BorderLayout.WEST);
        this.setVisible(true);
    }

    public boolean isGameStarted(){
        synchronized (this) {
            return gameStarted;
        }
    }

    public void setGameStarted(boolean bool){
        synchronized (this){
            gameStarted = bool;
        }
    }
}
