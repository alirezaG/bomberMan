package gamePackage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Player extends Block implements Animatable{
    private int bombExplosionTime, bombExplosionRange, bombLimit;  // in millisecond
    private int keyRespondTime;
    private final Map map;
    private int numberOfPlantedBombs;
    private PlayerStatus playerStatus;
    private int score = 0;
    private boolean controlBombStatus = false;

    private ArrayList<Bomb> bombs;


    Player(int x, int y, Map map, int bombExplosionTime, int bombExplosionRange, int keyRespondTime, int numberOfPlantedBombs, int bombLimit, int initialScore,
           boolean controlBombStatus){
        super(x, y);
        this.map = map;
        playerStatus = PlayerStatus.Stay;
        this.bombExplosionTime = bombExplosionTime;
        this.bombExplosionRange = bombExplosionRange;
        this.keyRespondTime = keyRespondTime;
        this.numberOfPlantedBombs = numberOfPlantedBombs;
        this.bombLimit = bombLimit;
        this.score = initialScore;
        this.controlBombStatus = controlBombStatus;
        bombs = new ArrayList<>();
    }

    public void setLocation(Point location){
        synchronized (this) {
            this.location = location;
        }
    }

    @Override
    public void step() { animation.animate(); }

    @Override
    public void save(BufferedWriter writer) {
        String str = "Player" + "#" + this.location.getX() + "#" + this.location.getY() + "#" + this.bombExplosionTime + "#" +
                + this.bombExplosionRange + "#" + this.numberOfPlantedBombs + "#" + this.keyRespondTime + "#\n";
        try { writer.write(str); } catch (IOException e) { e.printStackTrace(); }
    }


    public void move(Direction direction){
        int dx = 0, dy = 0;
        if (direction == Direction.Left) {
            dy = -1;
            playerStatus = PlayerStatus.MovingLeft;
        }
        else if (direction == Direction.Right) {
            dy = +1;
            playerStatus = PlayerStatus.MovingRight;
        }
        else if (direction == Direction.Up) {
            dx = -1;
            playerStatus = PlayerStatus.MovingUp;
        }
        else if (direction == Direction.Down) {
            dx = +1;
            playerStatus = PlayerStatus.MovingDown;
        }
        int destX = this.location.getX() + dx, destY = this.location.getY() + dy;

        if (map.getBlock(destX, destY) instanceof EmptyBlock)
            if (((EmptyBlock) map.getBlock(destX, destY)).hasBomb())
                return;
        if (map.getBlock(destX, destY) instanceof Stone || map.getBlock(destX, destY) instanceof Wall)  // bomb not working
            return;
        this.setLocation(new Point(destX, destY));
    }


    public void stay(){
        playerStatus = PlayerStatus.Stay;
    }


    // Functions for bomb
    public void plantBomb(Bomb bomb){
        if (getNumberOfPlantedBombs() >= bombLimit)
            return;   // cannot plant a bomb
        increaseNumberOfPlantedBombs();
        bombs.add(bomb);
    }


    public void die() {
        synchronized (this){
            playerStatus = PlayerStatus.Dead;
        }
    }


    public void eatPowerUp(PowerUp power){
        System.out.println(power.getType().toString() + " eaten");
        switch (power.getType()){
            case DecreaseSpeed:
                keyRespondTime = Math.min(keyRespondTime + 150, map.defaultMaximumKeyRespondTime);
                break;
            case DecreaseRadius:
                if (bombExplosionRange > 1)
                    bombExplosionRange--;
                break;
            case DecreasePoints:
                score -= 100;
                break;
            case DecreaseBombs:
                if (bombLimit > 1)
                    bombLimit--;
                break;
            case IncreaseSpeed:
                keyRespondTime = Math.max(keyRespondTime - 150, map.defaultMinimumKeyRespondTime);
            case IncreaseRadius:
                bombExplosionRange++;
                break;
            case IncreasePoints:
                score += 100;
                break;
            case ControlBomb:
                controlBombStatus = true;
                System.out.println("eaten ! ");
                break;
            case IncreaseBombs:
                bombLimit++;
                break;
            default:
        }
    }


    public void explodeFirstBomb(){
        if (!controlBombStatus)
            return;
        if (bombs.size() == 0)
            return;
        Bomb bomb = bombs.get(0);
        bomb.explode();
    }


    // Setters and Getters
    public int getKeyRespondTime(){
        synchronized (this){
            return keyRespondTime;
        }
    }

    public int getNumberOfPlantedBombs(){
        synchronized (this){
            return numberOfPlantedBombs;
        }
    }

    public void increaseNumberOfPlantedBombs(){
        synchronized (this){
            numberOfPlantedBombs++;
        }
    }

    public void decreaseNumberOfPlantedBombs(){
        synchronized (this){
            numberOfPlantedBombs--;
        }
    }

    public Bomb getBomb(int index){
        synchronized (this){
            return bombs.get(index);
        }
    }

    public void removeBomb(int index){
        synchronized (this){
            bombs.remove(index);
        }
    }

    public int getBombExplosionTime(){
        synchronized (this) {
            return bombExplosionTime;
        }
    }

    public int getBombExplosionRange(){
        synchronized (this){
            return bombExplosionRange;
        }
    }

    public PlayerStatus getPlayerStatus(){
        synchronized (this){
            return playerStatus;
        }
    }
}
