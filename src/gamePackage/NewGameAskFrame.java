package gamePackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class NewGameAskFrame extends JFrame {
    private JPanel contentPane;
    private JPanel btnPanel;
    private JTextField nRowsTxt, nColsTxt, nAliensTxt;
    private JButton startGameBtn, cancelBtn;
    private int numberOfRows, numberOfCols, numberOfAliens;

    NewGameAskFrame(){
        initialize();
    }

    private void initialize(){
        this.setSize(new Dimension(290, 240));
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        this.setLocation(100, 100);

        ImageIcon iconImg = new ImageIcon("./data/bomberManWithShield2.jpg");
        this.setIconImage(iconImg.getImage());
        this.setTitle("Input");

        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(new GridLayout(10, 1));
        nRowsTxt = new JTextField("15");
        nColsTxt = new JTextField("30");
        nAliensTxt = new JTextField("10");
        NewGameAskFrame thisFrame = this;

        startGameBtn = new JButton("Start!");
        startGameBtn.setPreferredSize(new Dimension(120, 45));
        startGameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               try{
                   numberOfRows = Integer.parseInt(nRowsTxt.getText());
                   numberOfCols = Integer.parseInt(nColsTxt.getText());
                   numberOfAliens = Integer.parseInt(nAliensTxt.getText());

                   if (numberOfRows <= 0 || numberOfCols <= 0)
                       throw new Throwable();
               } catch (Throwable exp) {
                   JOptionPane.showMessageDialog(contentPane, "You are only allowed to enter positive numbers!",
                           "Error", JOptionPane.ERROR_MESSAGE);
                   nRowsTxt.setText("");
                   nColsTxt.setText("");
                   nAliensTxt.setText("");
                   return;
               }
               thisFrame.dispatchEvent(new WindowEvent(thisFrame, WindowEvent.WINDOW_CLOSING));
            }
        });

        cancelBtn = new JButton("Cancel");
        cancelBtn.setPreferredSize(new Dimension(120, 45));
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                numberOfCols = numberOfRows = numberOfAliens = -1;  // means we shouldn't start the game
                thisFrame.dispatchEvent(new WindowEvent(thisFrame, WindowEvent.WINDOW_CLOSING));
            }
        });

        btnPanel = new JPanel(new BorderLayout());
        btnPanel.add(startGameBtn, BorderLayout.WEST);
        btnPanel.add(cancelBtn, BorderLayout.EAST);

        contentPane.add(Box.createRigidArea(new Dimension(200, 10)));
        contentPane.add(nRowsTxt);
        contentPane.add(Box.createRigidArea(new Dimension(200, 10)));
        contentPane.add(nColsTxt);
        contentPane.add(Box.createRigidArea(new Dimension(200, 10)));
        contentPane.add(nAliensTxt);
        contentPane.add(Box.createRigidArea(new Dimension(200, 10)));
        contentPane.add(btnPanel);
        contentPane.add(Box.createRigidArea(new Dimension(200, 10)));

        this.setVisible(true);
    }


    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfCols() {
        return numberOfCols;
    }

    public int getNumberOfAliens(){
        return numberOfAliens;
    }
}

