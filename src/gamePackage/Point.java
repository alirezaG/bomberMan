package gamePackage;

public class Point {
    private int x, y;

    Point(){
        x = y = 0;
    }
    Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    int getX(){ return x; }
    void setX(int x){ this.x = x; }
    int getY(){ return y; }
    void setY(int y) { this.y = y; }

    boolean equal(Point param){
        return x == param.x && y == param.y;
    }
}
