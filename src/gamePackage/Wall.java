package gamePackage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Wall extends Block implements Animatable{

    Wall(int x, int y){
        super(x, y);
    }

    @Override
    public void step() {
        animation.animate();
    }

    @Override
    public void save(BufferedWriter writer) {
        String str = "Wall" + "#" + this.location.getX() + "#" + this.location.getY() + "#\n";
        try { writer.write(str); } catch (IOException e) { e.printStackTrace(); }
    }

}
