package gamePackage;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class Block implements Animatable{
    Point location;
    AdvancedAnimation animation;

    Block(int x, int y){
        location = new Point(x, y);
        animation = null;
    }

    public void setAnimation(AdvancedAnimation anim){ animation = anim; }
    public void setLocation(Point location){ this.location = location; }

    public abstract void save(BufferedWriter writer);
}