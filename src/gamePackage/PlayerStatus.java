package gamePackage;

public enum PlayerStatus {
    MovingLeft, MovingRight, MovingUp, MovingDown, Stay, Dead
}
