package gamePackage;

public enum AlienStatus {
    MovingLeft(0), MovingRight(1), MovingUp(2), MovingDown(3), Stay(4), Dead(5), Disappeared(6), Undefined(7);

    private int key;

    AlienStatus(int k){
        this.key = k;
    }

    public int getKey(){
        return key;
    }

    static AlienStatus getStatus(int x){
        switch (x){
            case 6:
                return Disappeared;
            case 5:
                return Dead;
            case 4:
                return Stay;
            case 3:
                return MovingDown;
            case 2:
                return MovingUp;
            case 1:
                return MovingRight;
            case 0:
                return MovingLeft;
            default:
                return Undefined;
        }
    }
}
