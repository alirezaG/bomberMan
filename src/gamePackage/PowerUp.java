package gamePackage;

import java.io.BufferedWriter;

public class PowerUp extends Block implements Animatable{
    final private Map map;
    final private PowerUpType type;
    private boolean visible;

    PowerUp(int x, int y, Map map, PowerUpType powerUpType){
        super(x, y);
        this.map = map;
        this.type = powerUpType;
    }

    PowerUp(Point location, Map map, PowerUpType powerUpType){
        super(location.getX(), location.getY());
        this.location = location;
        this.map = map;
        this.type = powerUpType;
        this.visible = true;
    }

    @Override
    public void step() {
        animation.animate();
    }

    @Override
    public void save(BufferedWriter writer) {
        // To do
    }


    // Setters and Getters
    public PowerUpType getType(){
        synchronized (this){
            return type;
        }
    }

    public boolean isVisible() {
        synchronized (this){
            return visible;
        }
    }

    public void setVisible(boolean visible){
        synchronized (this){
            this.visible = visible;
        }
    }
}
