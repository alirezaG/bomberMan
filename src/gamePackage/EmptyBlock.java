package gamePackage;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class EmptyBlock extends Block{
    final Map map;
    private EmptyBlockStatus status, previousStatus;
    private int delayToExplosionCenter;

    EmptyBlock(int x, int y, Map map){
        super(x, y);
        status = previousStatus = EmptyBlockStatus.normal;
        this.map = map;
    }
    @Override
    public void step() { animation.animate(); }

    @Override
    public void save(BufferedWriter writer) {
        String str = "EmptyBlock" + "#" + this.location.getX() + "#" + this.location.getY() + "#\n";
        try { writer.write(str); } catch (IOException e) { e.printStackTrace(); }
    }


    boolean hasBomb(){
        for (int i = 0; i < map.getNumberOfPlayers(); i++)
            for (int j = 0; j < map.getPlayer(i).getNumberOfPlantedBombs(); j++) {
                Bomb bomb = map.getPlayer(i).getBomb(j);
                if (bomb.location.getX() == location.getX() && bomb.location.getY() == location.getY())
                    return true;
            }
        return false;
    }

    boolean hasAlien(){
        for (int i = 0; i < map.getNumberOfAliens(); i++) {
            Alien alien = map.getAlien(i);
            if (alien.location.getX() == this.location.getX() && alien.location.getY() == this.location.getY())
                return true;
        }
        return false;
    }

    boolean hasPowerUp(){
        for (int i = map.getNumberOfPowers() - 1; i >= 0; i--)
            if (map.getPower(i).location.equal(this.location))
                return true;
        return false;
    }

    // Setters and Getters
    public EmptyBlockStatus getStatus(){
        synchronized (this){
            return status;
        }
    }

    public EmptyBlockStatus getPreviousStatus(){
        synchronized (this){
            return previousStatus;
        }
    }

    public void setStatus(EmptyBlockStatus newStatus){
        synchronized (this){
            previousStatus = status;
            status = newStatus;
        }
    }

    public void setDelayToExplosionCenter(int delayToExplosionCenter){
        synchronized (this){
            this.delayToExplosionCenter = delayToExplosionCenter;
        }
    }

    public int getDelayToExplosionCenter(){
        synchronized (this){
            return delayToExplosionCenter;
        }
    }
}
